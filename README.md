# oss ncb

## How to start on podman

1. Git clone

```bash
$ git clone https://gitlab.com/flysangel/oss_ncb.git
```

2. Use oss ncb

```bash
$ cd oss_ncb
```

3. Setting .env

```bash
$ nano .env
::
EMAIL='qazzxc5200@gmail.com'
DOMAINS='gmm.flymks.com'
```

4. Run letsencrypt init

```bash
$ task letsencrypt:init
```

5. Change .env to prod

```bash
$ nano .env
::
STAGING='0'
```

6. Run init-letsencrypt

```bash
$ task letsencrypt:init
```

7. Change nginx default.conf

```bash
$ sudo nano ./volume/nginx/conf.d/default.conf
::
server {
    listen 443 ssl;

    server_name gmm.flymks.com;
    include /etc/nginx/policy/ssl/flymks.com.conf;

    location / {
        proxy_pass http://192.168.1.252:8080/guacamole/;
        include /etc/nginx/policy/websocket/websocket.conf;
        include /etc/nginx/policy/logger/logger.conf;
    }
}
```

8. Podman play

```bash
$ task ncb:deploy:podman
```

## License

[Apache License](./LICENSE).
